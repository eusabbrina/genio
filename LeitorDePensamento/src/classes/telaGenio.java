package classes;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
import javax.swing.JButton;
import javax.swing.SpinnerNumberModel;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class telaGenio extends JFrame {

	private JPanel contentPane;
	  

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					telaGenio frame = new telaGenio();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public telaGenio() {
		
		initComponents();
		
		inicializaComponentes();
		
	}
		
		
		
		
	private void initComponents() {
		// TODO Auto-generated method stub
		
	}

	private void inicializaComponentes() {	
		
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 442, 496);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(261, 134, 158, 274);
		lblNewLabel_1.setIcon(new ImageIcon(telaGenio.class.getResource("/imagens/akinator.png")));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.RIGHT);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblFrase = new JLabel("<html>Vou pensar em um valor entre 1 e 5. Tente adivinhar</html>");
		lblFrase.setFont(new Font("Tahoma", Font.BOLD, 12));
		lblFrase.setBounds(49, 71, 183, 68);
		contentPane.add(lblFrase);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(28, 11, 222, 187);
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setIcon(new ImageIcon(telaGenio.class.getResource("/imagens/balaopng.png")));
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_2 = new JLabel("Valor");
		lblNewLabel_2.setFont(new Font("Dialog", Font.BOLD, 12));
		lblNewLabel_2.setBounds(29, 311, 62, 14);
		contentPane.add(lblNewLabel_2);
		
		JSpinner txtVal = new JSpinner();
		txtVal.setModel(new SpinnerNumberModel(1, 1, 5, 1));
		txtVal.setBounds(115, 308, 40, 20);
		contentPane.add(txtVal);
		
		JButton btnPalpite = new JButton("Palpite");
		btnPalpite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				double n = 1 + Math.random() * (6 - 1 );
				int valor = (int) n;
				
				int num = Integer.parseInt(txtVal.getValue().toString()); 
				
				String f1 =  " ACERTOU! UHUUUU";
			    String f2 = " ERROU! Eu pensei no valor " + valor; 
			    
			    String rest = ( valor == num )? f1:f2; 
			    lblFrase.setText(rest);
						
			}
		});
		btnPalpite.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		btnPalpite.setBounds(37, 365, 103, 36);
		contentPane.add(btnPalpite);
	}
}
